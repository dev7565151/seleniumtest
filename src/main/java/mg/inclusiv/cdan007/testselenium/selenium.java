/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.inclusiv.cdan007.testselenium;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author ndimb
 */
public class selenium {
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\ndimb\\Downloads\\chromedriver-win64\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        //Attente que la page soit complète
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        
        driver.get("https://www.kibo.mg");
        
        // Effectuer des actions remplir champ de recherche et click sur la page
        driver.findElement(By.xpath("//*[@id=\"search_widget\"]/form/input[2]")).sendKeys("biberon");
        driver.findElement(By.xpath("//*[@id=\"search_widget\"]/form/button")).click();
        driver.findElement(By.xpath("//*[@id=\"js-product-list\"]/div[1]/article[1]/div/div[1]/div/a[2]")).click();
        driver.findElement(By.xpath("//*[@id=\"add-to-cart-or-refresh\"]/div[3]/div/div[1]/div/span[3]/button[1]/i")).click();
        driver.findElement(By.xpath("//*[@id=\"add-to-cart-or-refresh\"]/div[3]/div/div[2]/button")).click();
        
        // Attendre l'affichage du modal du panier
        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement cartModal = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"blockcart-modal\"]")));
        
        // Click sur le bouton pour accéder au panier
        cartModal.findElement(By.xpath("./div/div/div[2]/div/div[2]/div/div/a")).click();
        
        // Vérifier si la valeur "biberon" existe sur la page
        boolean articleExists = driver.findElements(By.xpath("//*[@id=\"main\"]/div[1]/div[1]/div[1]/div[2]/ul/li/div/div[2]/div[1]/a[contains(text(), 'biberon')]")).size() > 0;

        // Vérifier si la valeur "2" existe sur la page
        boolean quantiteExists = driver.findElements(By.xpath("//*[@id=\"main\"]/div[1]/div[1]/div[1]/div[2]/ul/li/div/div[3]/div/div[2]/div/div[1]/div/input[@value='2']")).size() > 0;

        if (articleExists && quantiteExists) {
            // Attendre l'affichage du bouton de validation de commande
            WebElement validateButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"main\"]/div[1]/div[2]/div/div[2]/div/a")));
            validateButton.click();
        }
        
        // Prendre une capture d'écran
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        
        try {
            FileUtils.copyFile(scrFile, new File("C:\\Users\\ndimb\\OneDrive\\Pictures\\captureSelenium\\screenshot.png"));
        } catch (IOException ex) {
            Logger.getLogger(selenium.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //driver.quit();
    }
}
