/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.inclusiv.cdan007.testselenium;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author ndimb
 */
public class NGTestNGTest {

    private WebDriver driver;

    @BeforeMethod
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\ndimb\\Downloads\\chromedriver-win64\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @AfterMethod
    public void tearDown() {
        //driver.quit();
    }

    @Test
    public void ngtest() {
        driver.get("https://www.kibo.mg");

        WebDriverWait wait = new WebDriverWait(driver, 10);
        
        // Recherche du produit "biberon"
        WebElement searchInput = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"search_widget\"]/form/input[2]")));
        searchInput.sendKeys("biberon");
        
        WebElement searchButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"search_widget\"]/form/button")));
        searchButton.click();
        
        // Sélection du premier produit de la liste
        WebElement productLink = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"js-product-list\"]/div[1]/article[1]/div/div[1]/div/a[2]")));
        productLink.click();
        
        // Ajout du produit au panier
        WebElement addToCartButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"add-to-cart-or-refresh\"]/div[3]/div/div[1]/div/span[3]/button[1]/i")));
        addToCartButton.click();
        
        WebElement continueShoppingButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"add-to-cart-or-refresh\"]/div[3]/div/div[2]/button")));
        continueShoppingButton.click();
        
        // Vérification du produit dans le panier
        WebElement cartButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"blockcart-modal\"]/div/div/div[2]/div/div[2]/div/div/a")));
        cartButton.click();
        
        boolean articleExists = driver.findElements(By.xpath("//*[@id=\"main\"]/div[1]/div[1]/div[1]/div[2]/ul/li/div/div[2]/div[1]/a[contains(text(), 'biberon')]")).size() > 0;
        boolean quantiteExists = driver.findElements(By.xpath("//*[@id=\"main\"]/div[1]/div[1]/div[1]/div[2]/ul/li/div/div[3]/div/div[2]/div/div[1]/div/input[@value='2']")).size() > 0;
        
        Assert.assertTrue(articleExists && quantiteExists);

        WebElement validateButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"main\"]/div[1]/div[2]/div/div[2]/div/a")));
        validateButton.click();

        // Capture d'écran
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

        try {
            FileUtils.copyFile(scrFile, new File("C:\\Users\\ndimb\\OneDrive\\Pictures\\captureSelenium\\screenshot.png"));
        } catch (IOException ex) {
            Logger.getLogger(NGTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }   
}